<?php
require 'vendor/autoload.php';
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);
ActiveRecord\Config::initialize(function($cfg){
    $cfg->set_model_directory('models');
    $cfg->set_connections(array('development' => 'mysql://root@localhost/murabbians'));
});

use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Topic;
use sngrl\PhpFirebaseCloudMessaging\Notification;

$authKey = function($request, $response, $next){
    if(isset($_GET['key'])){
      $data = Key::all(array('conditions' => array('api = ?', $_GET['key'])));
      $data = array_map(function($res){
        return $res->to_array();
      }, $data);
      if($data){
        $response = $next($request, $response);
      }else{
        _response($response, 401, 'unauthorized');
      }
    }else{
    }
    return $response;
};

$app->get('/api/v1/events', function ($request, $response) {
    $data = Event::all();
    $data = array_map(function($res){
      return $res->to_array();
    }, $data);
    _response($response, 200, 'success', $data);
})->add($authKey);

$app->get('/api/v1/recordings', function ($request, $response) {
    $data = Recording::all();
    $data = array_map(function($res){
      return $res->to_array();
    }, $data);
    _response($response, 200, 'success', $data);
});

$app->run();

// helper
function _response($response, $status = 404, $message = '', $data = ''){
  $temp = array('status' => $status, 'message' => $message);
  if($data != '') $temp = array('status' => $status, 'message' => $message, 'data' => $data);
  $response->withStatus($status)
           ->withHeader('Content-Type', 'application/json')
           ->withJson($temp, $status);
}
